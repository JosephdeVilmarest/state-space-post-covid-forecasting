import keras
import tensorflow as tf
import numpy
import pandas

# Definition of the MLP
def MLP_model(in_shape, k_list, a_list, drop_rate=0):
	inputs = keras.Input(shape=(in_shape,))
	fc = keras.layers.Dense(k_list[0], activation=a_list[0]) (inputs)
	if len(a_list) > 1:
		for k,a in zip(k_list[1:len(k_list)], a_list[1:len(k_list)]):
			fc = keras.layers.Dropout(drop_rate) (fc)
			fc = keras.layers.Dense(k, activation=a) (fc)
	outputs = keras.layers.Dense(1) (fc)
	model = keras.Model(inputs=inputs, outputs=outputs)
	return model

X = numpy.array(pandas.read_csv('data/Xaug_train.csv'))
y = numpy.array(pandas.read_csv('data/y_train.csv')).reshape(X.shape[0])

k_list = [15,10]
a_list = ['tanh','tanh']
N_days = X.shape[0] // 24
for i in range(24):
	print(i)
	sel = numpy.arange(i, X.shape[0], 24)
	model = MLP_model(X.shape[1], k_list, a_list, drop_rate=0.2)
	model.compile(optimizer=keras.optimizers.Adam(lr=0.001), loss='mean_squared_error')
	history = model.fit(X[sel], y[sel], batch_size = 64, epochs = 350, 
		verbose=0, validation_data = (X[sel], y[sel]))
	model.save('data/NNsave/NN-15-10-modelaug-'+str(i)+'.h5')